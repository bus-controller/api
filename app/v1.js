const express = require("express");

const AuthenticationRoutes = require("./routes/authentication.routes");
const UserRoutes = require("./routes/user.routes");
const CityRoutes = require("./routes/city.routes");
const BusRoutes = require("./routes/bus.routes");
const StatusRoutes = require("./routes/status.routes");
const LogRoutes = require("./routes/log.routes");

const app = express();

app.use("/auth", AuthenticationRoutes);
app.use("/user", UserRoutes);
app.use("/city", CityRoutes);
app.use("/bus", BusRoutes);
app.use("/status", StatusRoutes);
app.use("/logs", LogRoutes);

module.exports = app;