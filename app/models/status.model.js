const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const statusModel = new Schema(
    {
        name: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        label: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        sort: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        }
    },
    {
        timestamps: true,
    }
);

const Status = mongoose.model("status", statusModel);

module.exports = Status;