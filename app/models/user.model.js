const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userModel = new Schema(
    {
        name: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        otp: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        phone: {
            type: String,
            unique: true,
            required: true,
            default: "",
        },
        password: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        sex: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        access: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
    },
    {
        timestamps: true,
    }
);

const User = mongoose.model("user", userModel);

module.exports = User;