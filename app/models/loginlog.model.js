const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const loginLog = new Schema(
    {
        ip: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        date: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        time: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        user: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        type: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
    },
    {
        timestamps: true,
    }
);

const LoginLog = mongoose.model('login log', loginLog);

module.exports = LoginLog;