const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const busLog = new Schema(
    {
        from: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        to: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        message: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        bus: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        date: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        time: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        project: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        user: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
    },
    {
        timestamps: true,
    }
);

const BusLog = mongoose.model('bus log', busLog);

module.exports = BusLog;