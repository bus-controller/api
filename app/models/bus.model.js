const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const busModel = new Schema(
    {
        name: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        city: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        details: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        status: {
            type: String,
            unique: false,
            required: false,
            default: "parking",
        },
        color: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        gearbox: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        chassis: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        plaque: {
            type: String,
            unique: true,
            required: true,
            default: "",
        },
        ac: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        model: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        delivered: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        enter: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        ceo_command: {
            type: String,
            unique: false,
            required: false,
            default: "دستورات خاص",
        },
        deleted: {
            type: Boolean,
            unique: false,
            required: false,
            default: false,
        },
    },
    {
        timestamps: true,
    }
);

const Bus = mongoose.model("bus", busModel);

module.exports = Bus;