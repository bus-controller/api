const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const cityModel = new Schema(
    {
        name: {
            type: String,
            unique: false,
            required: true,
            default: "",
        },
        color: {
            type: String,
            unique: false,
            required: false,
            default: "",
        },
        budget: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        sazehBudget: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        niroBudget: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        barghBudget: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        tazinatBudget: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        colorBudget: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        etcBudget: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        sazehBudgetSpent: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        niroBudgetSpent: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        barghBudgetSpent: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        tazinatBudgetSpent: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        colorBudgetSpent: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
        etcBudgetSpent: {
            type: Number,
            unique: false,
            required: false,
            default: 0,
        },
    },
    {
        timestamps: true,
    }
);

const City = mongoose.model("city", cityModel);

module.exports = City;