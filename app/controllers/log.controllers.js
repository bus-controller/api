const mongoose = require("mongoose");
const fs = require('fs');

const Bus = require("../models/bus.model");
const City = require("../models/city.model");
const BusLog = require("../models/buslog.model");
const LoginLog = require("../models/loginlog.model");

require("dotenv").config();
const env = process.env;

const BusLogs = (req, res) => {
    BusLog.find()
        .select("-updatedAt -createdAt -month -__v")
        .then((result) => {
            res.status(200).send({ logs: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const LoginLogs = (req, res) => {
    LoginLog.find()
        .select("-updatedAt -createdAt -__v")
        .then((result) => {
            res.status(200).send({ logs: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const ListModels = (req, res) => {
    const names = mongoose.modelNames();

    res.status(200).send({ models: names });
}

const BackupModels = (req, res) => {
    const { model } = req.body;

    const models = mongoose.models;
    const select = models[model];

    select.find()
        .then((result) => {
            const date = new Date();

            const naming = `${date.getFullYear()}-${date.getMonth()}-${date.getDay()}-${date.getHours()}-${date.getMinutes()}-${model}`;

            if (env.PRODUCTION) {
                const file = fs.createWriteStream(`/tmp/backups/${naming}.json`);
                file.write(JSON.stringify(result));
            }

            res.status(200).send({ message: "بک‌آپ گرفته شد", data: result, naming });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const ListProjects = (req, res) => {
    City.find()
        .then((result) => {
            res.status(200).send({ projects: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const BackupProject = (req, res) => {
    const { id } = req.body;

    City.findById(id)
        .select("-updatedAt -createdAt -__v")
        .then((project) => {
            Bus.find({ city: project.id })
                .select("-updatedAt -city -createdAt -__v")
                .then((busses) => {
                    res.status(200).send({
                        message: "گزارش گرفته شد",
                        project,
                        busses
                    });
                })
                .catch((error) => {
                    res.status(500).send({ error });
                });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

module.exports = {
    ListModels,
    BackupModels,
    ListProjects,
    BackupProject,
    BusLogs,
    LoginLogs,
}