const Bus = require("../models/bus.model");
const City = require("../models/city.model");
const BusLog = require("../models/buslog.model");

const NewBus = (req, res) => {
    const { data, log } = req.body;

    const city = data.city;
    
    City.findById(city)
        .then((result) => {
            data['color'] = result.color;

            const bus = new Bus(data);
            bus.save()
                .then((result) => {
                    const busLog = new BusLog(log);
                    busLog.save()
                        .then((saved) => res.status(200).send({ message: "اتوبوس با موفقیت شد" }))
                        .catch((error) => res.status(500).send({ error }));
                })
                .catch((error) => res.status(500).send({ error }));
        })
        .catch((error) => {
            res.status(500).send({ error });
        });

    
}

const AllBuses = (req, res) => {
    Bus.find({ deleted: false })
        .then((result) => {
            res.status(200).send({ buses: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const DeletedBuses = (req, res) => {
    Bus.find({ deleted: true })
        .then((result) => {
            res.status(200).send({ buses: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const GetOne = (req, res) => {
    const { id } = req.params;

    Bus.findById(id)
        .then((result) => {
            res.status(200).send({ bus: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const UpdateBus = (req, res) => {
    const { id, data, log } = req.body;

    Bus.findByIdAndUpdate(id, data)
        .then((result) => {
            const busLog = new BusLog(log);
            busLog.save()
                .then((saved) => res.status(200).send({ message: "اتوبوس آپدیت شد" }))
                .catch((error) => res.status(500).send({ error }));
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const DeleteBus = (req, res) => {
    const { id, log } = req.body;

    Bus.findByIdAndUpdate(id, { deleted: true })
        .then((result) => {
            const busLog = new BusLog(log);
            busLog.save()
                .then((saved) => res.status(200).send({ message: "اتوبوس حذف شد" }))
                .catch((error) => res.status(500).send({ error }));
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const RestoreBus = (req, res) => {
    const { id, log } = req.body;

    Bus.findByIdAndUpdate(id, { deleted: false })
        .then((result) => {
            const busLog = new BusLog(log);
            busLog.save()
                .then((saved) => res.status(200).send({ message: "اتوبوس به سیستم باز گردید" }))
                .catch((error) => res.status(500).send({ error }));
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const fixIssue = (req, res) => {
    Bus.updateMany({ deleted: false })
        .then((result) => {
            res.status(200).send({ message: "مشکل حل شد" });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

module.exports = {
    NewBus,
    AllBuses,
    DeletedBuses,
    UpdateBus,
    GetOne,
    DeleteBus,
    RestoreBus,
    fixIssue,
}