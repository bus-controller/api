const Status = require("../models/status.model");

const NewStatus = (req, res) => {
    const data = req.body;

    Status.find()
        .then((result) => {
            const status = new Status({
                sort: result.length,
                ...data,
            });
        
            status.save()
                .then((result) => res.status(200).send({ message: "وضعیت با موفقیت ساخته شد" }))
                .catch((error) => res.status(500).send({ error }));
        })
        .catch((error) => {
            res.status(500).send({ error });
        });

    
}

const AllStatus = (req, res) => {
    Status.find().sort({ sort: "asc" })
        .then((result) => {
            res.status(200).send({ statuses: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const GetOne = (req, res) => {
    const { id } = req.params;

    Status.findById(id)
        .then((result) => {
            res.status(200).send({ status: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const DeleteStatus = (req, res) => {
    const { id } = req.params;

    Status.findByIdAndDelete(id)
        .then((result) => {
            res.status(200).send({ message: "دسته بندی حذف شد" });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const UpdateStatus = (req, res) => {
    const { id, data } = req.body;

    Status.findByIdAndUpdate(id, data)
        .then((result) => {
            res.status(200).send({ message: "دسته بندی آپدیت شد" });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

module.exports = {
    NewStatus,
    AllStatus,
    GetOne,
    DeleteStatus,
    UpdateStatus,
}