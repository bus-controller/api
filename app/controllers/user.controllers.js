const User = require("../models/user.model");

const GetOne = (req, res) => {
    const { id } = req.params;

    User.findById(id)
        .then((result) => {
            res.status(200).send({ user: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const GetUsers = (req, res) => {
    User.find()
        .then((result) => {
            res.status(200).send({ users: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const DeleteUser = (req, res) => {
    const { id } = req.params;

    User.findByIdAndDelete(id)
        .then((result) => {
            res.status(200).send({ message: "کاربر حذف شد" });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const UpdateUser = (req, res) => {
    const { id, data } = req.body;

    User.findByIdAndUpdate(id, data)
        .then((result) => {
            res.status(200).send({ message: "کاربر آپدیت شد" });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

module.exports = {
    GetOne,
    GetUsers,
    DeleteUser,
    UpdateUser,
}