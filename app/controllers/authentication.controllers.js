const PayamSMS = require("payamsms-sdk");
const LoginLog = require("../models/loginlog.model");

const User = require("../models/user.model");

require("dotenv").config();
env = process.env;

const sms = new PayamSMS(
    env.PAYAM_ORGANIZATION,
    env.PAYAM_USERNAME,
    env.PAYAM_PASSWORD,
    env.PAYAM_LINE
);


const makePasscode = (length) => {
    var result = '';
    var characters = '0123456789';
    var charactersLength = characters.length;

    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(
            Math.floor(
                Math.random() * charactersLength
            )
        );
    }

   return result;
}

const GenerateOTP = (req, res) => {
    const { phone } = req.body;

    const newCode = makePasscode(6);

    User.findOneAndUpdate({ phone }, { otp: newCode })
        .then((result) => {
            if (result !== null) {                
                const messages = [
                    {
                        recipient: result.phone,
                        body: newCode,
                    },
                ];

                const send = sms.send(messages);
                send.then((sent) => {
                    const message = sent[0];
    
                    if (message.code === 200) res.status(200).send({ message: "کد ارسال شد" });
                    else res.status(200).send({ message: "کد ارسال نشد" })
                });
            }
            else res.status(401).send({ message: "کاربر پیدا نشد" });
        })
        .catch((error) => res.status(500).send({ error }));
}

const CheckOTP = (req, res) => {
    const { phone, passcode } = req.body;

    User.findOneAndUpdate({ phone, otp: passcode }, { otp: "" })
        .then((result) => res.status(200).send({ message: "کاربر وجود دارد", id: result.id, access: result.access }))
        .catch((error) => res.status(401).send({ message: "کد وارد شده معتبر نیست" }));
}

const RegisterUser = (req, res) => {
    const data = req.body;

    const user = new User(data);

    user.save()
        .then((result) => res.status(200).send({ message: "کاربر با موفقیت ساخته شد" }))
        .catch((error) => res.status(500).send({ error }));
}

const LoginUser = (req, res) => {
    const data = req.body;

    User.findOne(data)
        .then((result) => {
            const date = new Date();

            const loginData = {
                user: result.name,
                date: date.toLocaleDateString('fa-IR'),
                time: date.toLocaleTimeString('fa-IR'),
                ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                type: "login",
            };

            const newLog = new LoginLog(loginData);
            newLog.save()
                .then((saved) => res.status(200).send({ message: "کاربر وجود دارد", id: result.id, access: result.access }))
                .catch((error) => res.status(500).send({ error }))
        })
        .catch((error) => res.status(401).send({ message: "کاربر پیدا نشد" }));
}

const LogoutUser = (req, res) => {
    const { uid } = req.body;

    User.findById(uid)
        .then((result) => {
            const date = new Date();

            const logoutData = {
                user: result.name,
                date: date.toLocaleDateString('fa-IR'),
                time: date.toLocaleTimeString('fa-IR'),
                ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                type: "logout",
            };

            const newLog = new LoginLog(logoutData);
            newLog.save()
                .then((saved) => res.status(200).send({ message: "کاربر وجود دارد" }))
                .catch((error) => res.status(500).send({ error }))
        })
        .catch((error) => res.status(401).send({ message: "کاربر پیدا نشد" }));
}

module.exports = {
    GenerateOTP,
    CheckOTP,
    RegisterUser,
    LoginUser,
    LogoutUser,
}