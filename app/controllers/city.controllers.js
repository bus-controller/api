const City = require("../models/city.model");
const Bus = require("../models/bus.model");

const NewCity = (req, res) => {
    const data = req.body;

    const city = new City(data);

    city.save()
        .then((result) => res.status(200).send({ message: "شهر با موفقیت ساخته شد" }))
        .catch((error) => res.status(500).send({ error }));
}

const AllCity = (req, res) => {
    City.find()
        .then((result) => {
            res.status(200).send({ cities: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const GetOne = (req, res) => {
    const { id } = req.params;

    City.findById(id)
        .then((result) => {
            res.status(200).send({ city: result });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const DeleteCity = (req, res) => {
    const { id } = req.params;

    City.findByIdAndDelete(id)
        .then((result) => {
            res.status(200).send({ message: "پروژه حذف شد" });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

const UpdateCity = (req, res) => {
    const { id, data } = req.body;

    City.findByIdAndUpdate(id, data)
        .then((result) => {
            Bus.updateMany({ city: result.id }, { color: data.color })
                .then((response) => {
                    res.status(200).send({ message: "پروژه آپدیت شد" });
                })
                .catch((error) => {
                    res.status(500).send({ error });
                });
        })
        .catch((error) => {
            res.status(500).send({ error });
        });
}

module.exports = {
    NewCity,
    AllCity,
    GetOne,
    DeleteCity,
    UpdateCity,
}