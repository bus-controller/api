const express = require("express");

const controllers = require("../controllers/bus.controllers");

const Router = express.Router();

Router.post("/new", controllers.NewBus);
Router.post("/delete", controllers.DeleteBus);
Router.post("/restore", controllers.RestoreBus);
Router.post("/update", controllers.UpdateBus);
Router.get("/all", controllers.AllBuses);
Router.get("/all/deleted", controllers.DeletedBuses);
Router.get("/one/:id", controllers.GetOne);
Router.get("/fix", controllers.fixIssue);

module.exports = Router;