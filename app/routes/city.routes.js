const express = require("express");

const controllers = require("../controllers/city.controllers");

const Router = express.Router();

Router.post("/new", controllers.NewCity);
Router.delete("/delete/:id", controllers.DeleteCity);
Router.post("/update", controllers.UpdateCity);
Router.get("/all", controllers.AllCity);
Router.get("/one/:id", controllers.GetOne);

module.exports = Router;