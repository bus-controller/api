const express = require("express");

const controllers = require("../controllers/log.controllers");

const Router = express.Router();

Router.get("/backup/model/list", controllers.ListModels);
Router.post("/backup/model", controllers.BackupModels);

Router.get("/backup/project/list", controllers.ListProjects);
Router.post("/backup/project", controllers.BackupProject);

Router.get("/bus/logs", controllers.BusLogs);
Router.get("/login/logs", controllers.LoginLogs);

module.exports = Router;