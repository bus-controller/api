const express = require("express");

const controllers = require("../controllers/authentication.controllers");

const Router = express.Router();

Router.post("/otp/generate", controllers.GenerateOTP);
Router.post("/otp/check", controllers.CheckOTP);
Router.post("/register", controllers.RegisterUser);
Router.post("/login", controllers.LoginUser);
Router.post("/logout", controllers.LogoutUser);

module.exports = Router;