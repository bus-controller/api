const express = require("express");

const controllers = require("../controllers/user.controllers");

const Router = express.Router();

Router.delete("/delete/:id", controllers.DeleteUser);
Router.post("/update", controllers.UpdateUser);
Router.post("/all", controllers.GetUsers);
Router.get("/user/:id", controllers.GetOne);

module.exports = Router;