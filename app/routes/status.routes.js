const express = require("express");

const controllers = require("../controllers/status.controllers");

const Router = express.Router();

Router.post("/new", controllers.NewStatus);
Router.delete("/delete/:id", controllers.DeleteStatus);
Router.post("/update", controllers.UpdateStatus);
Router.get("/all", controllers.AllStatus);
Router.get("/one/:id", controllers.GetOne);

module.exports = Router;